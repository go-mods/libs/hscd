# hscd

[![codecov](https://codecov.io/gh/./branch/master/graph/badge.svg)](https://codecov.io/gh/.)
[![GoDoc](https://img.shields.io/badge/pkg.go.dev-doc-blue)](http://pkg.go.dev/.)

The hscd (hash code) package offers various implementations of
algorithms to generate hash codes from arbitrary data (based on
[https://archive.vn/KJeJy#selection-4260.0-4260.1](https://archive.vn/KJeJy#selection-4260.0-4260.1)).

## Variables

The length of computed hash codes in bytes (default: 4).

```golang
var HashLen = 4
```

## Functions

### func [DJB](/hscd.go#L15)

`func DJB(key []byte) []byte`

DJB implements a slightly modified version of the 'Chris Torek
hash algorithm' by Dan Bernstein (XOR instead of ADD).

### func [ELF](/hscd.go#L124)

`func ELF(key []byte) []byte`

ELF implements a hash algorithm that in addition of shifting
makes use of the AND and NOT operators.

### func [FNV](/hscd.go#L56)

`func FNV(key []byte) []byte`

The FNV hash, short for Fowler/Noll/Vo in honor of the creators,
is a very powerful algorithm that, not surprisingly, follows the
same lines as Bernstein's modified hash with carefully chosen
constants.

### func [JEN](/hscd.go#L160)

`func JEN(key, init []byte) []byte`

JEN implements the 'Jenkins hash algorithm' by Bob Jenkins.

### func [JSW](/hscd.go#L100)

`func JSW(key []byte) []byte`

JSW implements a hash algorithm that uses a table of 256 random
generated numbers to reach a good distribution and performance
(by Julienne Walker).

### func [OAT](/hscd.go#L75)

`func OAT(key []byte) []byte`

OAT implements the 'One-at-a-Time hash algorithm' by Bob Jenkins.

### func [SAX](/hscd.go#L34)

`func SAX(key []byte) []byte`

SAX implements the 'shift-add-xor hash algorithm' which was
designed as a string hashing function.

---
Readme created from Go doc with [goreadme](https://github.com/posener/goreadme)
